import './../App.css'

import React from 'react'

const Button = ({children, cssClass, onClick}) => {
    return (
            <button className={cssClass} onClick={onClick}>
                { children }
            </button>
    )
}

export default Button;
