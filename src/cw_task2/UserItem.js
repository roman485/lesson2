import Button from '../cw_task1/Button'
import React, { useState } from 'react'

const UserItem = ({ user }) => {
    const [isActive, setActive] = useState(false)

    return (
        <div>
            <li className={isActive?'status__arrived':'status_notarrived'}>
                {user.name} <b/>
                <Button children='Interview' cssClass="btn" onClick={() => setActive(!isActive)}></Button>
            </li>
        </div>
)
}

export default UserItem;

