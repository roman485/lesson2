import React, {Component} from 'react'
import UserItem from './UserItem';


class UserList extends Component
{
    constructor( props ){
        console.log('[Lifecycle]. Constructor '); 
        super( props ); 
        this.state = {
            users: [], 
            isActive: false
        }
    }

    componentDidMount = () => {
        console.log('[Lifecycle] 3. componentDidMount '  );
        this.fetching();
    } 

    render = () => {
        console.log('[Lifecycle]. Render');
        const {users, isActive} = this.state;
        console.log('re.isActive: ' + isActive);
        if( users.length === 0){
            return(
                <span>Loading...</span>
            )
        }
        return(
            <ul>
                {
                    users.map( user=> {
                        return(
                                <UserItem key={user.index} user={user.user}></UserItem>
                        )
                    })
                }
            </ul>
        )
    }



    fetching(){
        fetch('http://www.json-generator.com/api/json/get/cdZfJCjIia?indent=2')
        .then(response => response.json())
        .then(json => {
            this.setState({
                users: json
            })
        });    
    }
    

}

export default UserList; 
