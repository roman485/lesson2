import React, {Component} from 'react'
import './../App.css'
import loader from './loader.gif'

class LoaderImg extends Component
{
    constructor( props ){
        super( props ); 

        this.state = {
            isLoad: false,
            err: null,
            imgUrl: ''
        }
    }

    componentDidMount = () => {
        
    fetch(this.props.src)
        .then(response => response.blob())
        .then(image => {
            this.setState({isLoad: true});
            this.setState({imgUrl: URL.createObjectURL(image)});
        })
        .then((error) => {
            this.setState({isLoad: true});
            this.setState({err: error});
        })
    } 

    render = () => {
        
        const { isLoad, err, imgUrl } = this.state;
        
        if (!isLoad) return <div><img src={loader} className='img_loader'></img></div>;
        if (err) return <div>Error: {err.message}</div>;
        if (imgUrl == '') return <div>No photo</div>;

        return(
            <div>
                <img src={imgUrl} className='img'></img>
            </div>
        )
    }
}

export default LoaderImg; 
