import React, {Component} from 'react'
import Button from './cw_task1/Button'
import UserList from './cw_task2/UserList'
import './App.css';
import LoaderImg from './hw_task1/Img'
import Table from './hw_task2/Table'
import Row from './hw_task2/Row';
import Cell from './hw_task2/Cell';

class App extends Component {

  handleClick = () => {
    console.log('Button click');
    alert('Button click');
  }

  render() {
    const children = <b>Knopka</b>
    const btn = <Button children={children} cssClass="btn" onClick={this.handleClick}></Button>;

    return (
    <div className="App">
      <h5>Class work 1</h5>
      {btn}
  
      <h5>Class work 2</h5>
      <UserList></UserList>

      <h5>Home work 1</h5>
      <LoaderImg src = "https://dummyimage.com/5000x3000/acc/eee.jpg&text=ITEA-ReactJS-Lesson2" ></LoaderImg>

      <h5>Home work 2</h5>
      <Table>
        <Row head="true">
          <Cell type="">#</Cell>
          <Cell type="date">date</Cell>
          <Cell type="number">number</Cell>
          <Cell type="money" >money</Cell>
        </Row>
        <Row>
          <Cell type="" background="gray">1</Cell>
          <Cell type="date">11.02.2015</Cell>
          <Cell type="number" color="red">3123</Cell>
          <Cell type="money" currency="$">400</Cell>
      </Row>
      <Row>
          <Cell type="" >2</Cell>
          <Cell type="date">14.05.2016</Cell>
          <Cell type="number" background="green">356</Cell>
          <Cell type="money" color="blue" currency="$">700</Cell>
      </Row>
      </Table>

    </div>

    )
  }
}



export default App; 
