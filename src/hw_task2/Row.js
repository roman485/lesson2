import React from 'react'
import './Table.css'

const Row = ({head, children}) => {

    const rowClass = head?"row head":"row"

    return (
        <div className={rowClass}>
            
                {React.Children.map(children, child => {
                    return (child)
                }
                )
            }
        </div>
    )
}

Row.defaultProps = {
    head: false
} 

export default Row;