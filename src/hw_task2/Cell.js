import React from 'react'

const Cell = ({children, type, background, color, currency }) => {

    let typeStyle = {}
    let fontStyle = { background: background, color: color }

    if(type === 'date' )
    {
        typeStyle = { fontStyle: 'italic' } 
    }
    else if(type === 'number')
    {
        typeStyle = { textAlign: 'right' }
    }
    else if(type === 'money')
    {
        typeStyle = { textAlign: 'right' }
        if(currency != null){
            children =  children + currency;
        }
        else{
            console.log('No currency'); 
        }
    }

    const cellStyle = { ...typeStyle, ...fontStyle } 

    return (
        <div className={'cell'} style={cellStyle}>
            {children}
        </div>
    )
}

Cell.defaultProps = {
    type: "date",
    background: 'transparent',
    color: 'black'  
}

export default Cell;